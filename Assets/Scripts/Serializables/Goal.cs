﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Goal
{
    private string goalName;
    private int goalPrice;
    private int progress = 0;

    public Goal(string _goalName, int _goalPrice)
    {
        goalName = _goalName;
        goalPrice = _goalPrice;
    }

    public string GetGoalName() { return goalName; }
    public int GetGoalPrice() { return goalPrice; }

    public void SetProgress(int _price) { progress = _price; }
    public int GetProgress() { return progress; }
}
