﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ExpenseProfile
{
    public string expenseName = string.Empty;
    public int expenseCost = 0;

    public ExpenseProfile(string _Name, int _Cost)
    {
        expenseName = _Name;
        expenseCost = _Cost;
    }
}