﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BudgetSheet
{
    private int totalBudget = 0;
    private int remainingBudget = 0;
    private int totalSpent = 0;
    private string budgetName = string.Empty;

    private List<ExpenseProfile> expenses = new List<ExpenseProfile>();

    public BudgetSheet(int _TotalBudget, string _BudgetName)
    {
        totalBudget = _TotalBudget;
        remainingBudget = _TotalBudget;
        budgetName = _BudgetName;
    }

    public void AddExpense(string _ExpenseName, int _Cost)
    {
        ExpenseProfile expense = new ExpenseProfile(_ExpenseName, _Cost);
        remainingBudget -= _Cost;
        totalSpent += _Cost;
        expenses.Add(expense);
        AppManager.instance.uiManager.sheetMenu.UpdateSheet();
    }

    public void RemoveLastExpense()
    {
        expenses.Remove(expenses[expenses.Count -1]);
        UpdateBudget();
    }

    private void UpdateBudget()
    {
        remainingBudget = totalBudget;
        totalSpent = 0;
        for(int i = 0; i < expenses.Count; i++)
        {
            remainingBudget -= expenses[i].expenseCost;
            totalSpent += expenses[i].expenseCost;
        }
    }

    public int GetTotalBudget() { return totalBudget; }
    public string GetBudgetName() { return budgetName; }
    public int GetRemainingBudget() { return remainingBudget; }
    public void SetRemainingBudget(int newRemaining) { remainingBudget = newRemaining; }
    public int GetTotalSpent() { return totalSpent; }
    public List<ExpenseProfile> GetAllExpenses() { return expenses; }
}
