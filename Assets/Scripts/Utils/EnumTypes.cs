﻿public enum AppMode
{
    MAINMENU,
    SHEET,
    Goal,
    Settings
}

public enum DisplaySettings
{
    OriginalGreen,
    Blue,
    Dark
}