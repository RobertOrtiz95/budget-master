﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringIndex : MonoBehaviour {

    public static string GetSheetsFolderPath()
    {
        string folderPath = Application.persistentDataPath + "/" + "BudgetSheets";
        return folderPath;
    }
    public static string GetGoalFolderPath()
    {
        string folderPath = Application.persistentDataPath + "/" + "Goals";
        return folderPath;
    }
    public static string GetBudgetSheetFileName(string fileName)
    {
        string file = Application.persistentDataPath + "/" + "BudgetSheets/" + fileName + ".dat";
        return file;
    }

    public static string GetGoalFileName(string fileName)
    {
        string file = Application.persistentDataPath + "/" + "Goals/" + fileName + ".goal";
        return file;
    }

    public static string GetCurrency(int _Amount)
    {
        string currency = "$" + _Amount.ToString();
        return currency;
    }

    public static string GetTotalBudgetCurrency(int _Amount)
    {
        string totalBudget = "Total : $" + _Amount.ToString();
        return totalBudget;
    }

    public static string GetRemainingBudgetCurrency(int _Amount)
    {
        string remainingBudget = "Remaining : $" + _Amount;
        return remainingBudget;
    }

    public static string GetTotalSpentCurrency(int _Amount)
    {
        string totalSpent = "Spent : $" + _Amount.ToString();
        return totalSpent;
    }
}
