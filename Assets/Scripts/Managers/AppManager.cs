﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{
    public static AppManager instance = null;
    public static EventHandlerAppMode appModeEvent;
    private static AppMode _appMode;
    public static AppMode appMode { get { return _appMode; }
        set {
            _appMode = value;
            if (appModeEvent != null)
                appModeEvent(_appMode);
        }
    }

    public BudgetManager budgetManager = null;
    public UIManager uiManager = null;
    public MemoryManager memoryManager = null;

    private void Start()
    {
        instance = this;
        appMode = AppMode.MAINMENU;
        uiManager.mainMenu.LoadPreviousSheets();
    }

    public void SetAppMode(int _mode)
    {
        switch (_mode)
        {
            case 0:
                appMode = AppMode.MAINMENU;
                break;
            case 1:
                appMode = AppMode.Goal;
                break;
            case 2:
                appMode = AppMode.Settings;
                break;
        }
    }
}
