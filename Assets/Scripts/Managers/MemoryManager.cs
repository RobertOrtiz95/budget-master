﻿using System;
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class MemoryManager : MonoBehaviour
{

    #region Inital SetUp Functions

    private void OnEnable()
    {
        CreateFolders();
        GetFilesInSheetsFolder();
    }

    private void CreateFolders()
    {
        Directory.CreateDirectory(StringIndex.GetSheetsFolderPath());
        Directory.CreateDirectory(StringIndex.GetGoalFolderPath());
    }

    public static string[] GetFilesInSheetsFolder()
    {
        string[] filePaths = Directory.GetFiles(StringIndex.GetSheetsFolderPath() + "/", "*.dat");
        return filePaths;
    }

    public static string[] GetFilesInGoalsFolder()
    {
        string[] filePaths = Directory.GetFiles(StringIndex.GetGoalFolderPath() + "/", "*.goal");
        return filePaths;
    }

    #endregion

    #region budget Data Management

    public static void SaveBudgetSheet(BudgetSheet budgetSheet)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(StringIndex.GetBudgetSheetFileName(budgetSheet.GetBudgetName()));
        BudgetSheet temp = budgetSheet;
        bf.Serialize(file, temp);
        file.Close();
    }

    public static BudgetSheet LoadBudgetSheet(string fileName)
    {
        if (File.Exists(fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fileName, FileMode.Open);
            BudgetSheet budgetData = (BudgetSheet)bf.Deserialize(file);
            file.Close();
            return budgetData;
        }
        else
            return null;
    }

    public void DeleteBudgetSheet(string fileName) { File.Delete(StringIndex.GetBudgetSheetFileName(fileName)); }
    
    #endregion

    #region goal data management

    public static void SaveGoal(Goal _goal)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(StringIndex.GetGoalFileName(_goal.GetGoalName()));
        Goal goalData = _goal;
        bf.Serialize(file, goalData);
        file.Close();
    }

    public static Goal LoadGoal(string fileName)
    {
        if (File.Exists(fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fileName, FileMode.Open);
            Goal goalData = (Goal)bf.Deserialize(file);
            file.Close();
            return goalData;
        }
        else
            return null;
    }

    public static void DeleteGoalData(string fileName) { File.Delete(StringIndex.GetGoalFileName(fileName)); }

    #endregion

}
