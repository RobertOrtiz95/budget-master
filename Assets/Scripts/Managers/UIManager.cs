﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public MainMenu mainMenu = null;
    public SheetMenu sheetMenu = null;
    public GoalMenu goalMenu = null;
    public GameObject subMenu = null;
    public GameObject settingsMenu = null;

    private void OnEnable()
    {
        AppManager.appModeEvent += UpdateUI;
    }

    private void OnDisable()
    {
        AppManager.appModeEvent -= UpdateUI;
    }

    private void UpdateUI(AppMode _currentAppMode)
    {
        ResetUI();
        switch (_currentAppMode)
        {
            case AppMode.MAINMENU:
                mainMenu.gameObject.SetActive(true);
                mainMenu.LoadPreviousSheets();
                break;
            case AppMode.SHEET:
                sheetMenu.gameObject.SetActive(true);
                sheetMenu.CreateBudgetSheet();
                break;
            case AppMode.Goal:
                goalMenu.gameObject.SetActive(true);
                goalMenu.LoadGoals();
                break;
            case AppMode.Settings:
                settingsMenu.SetActive(true);
                break;
        }
    }
    
    private void ResetUI()
    {
        mainMenu.gameObject.SetActive(false);
        sheetMenu.gameObject.SetActive(false);
        goalMenu.gameObject.SetActive(false);
        subMenu.SetActive(false);
        settingsMenu.SetActive(false);
    }
}
