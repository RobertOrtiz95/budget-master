﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BudgetManager : MonoBehaviour
{
    public BudgetSheet currentBudgetSheet = null;

    public void CreateNewSheet(int _TotalBudget, string _BudgetName)
    {
        currentBudgetSheet = new BudgetSheet(_TotalBudget, _BudgetName);
        MemoryManager.SaveBudgetSheet(currentBudgetSheet);
    }

    public void AddNewExpense(ExpenseProfile profile)
    {
        if (currentBudgetSheet != null)
            currentBudgetSheet.AddExpense(profile.expenseName, profile.expenseCost);
    }

    public void BudgetToGoal()
    {
        if (currentBudgetSheet == null)
            return;


    }
    
}
