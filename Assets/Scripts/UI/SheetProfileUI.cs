﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SheetProfileUI : MonoBehaviour
{
    public TMP_Text sheetName = null;
    public TMP_Text budgetTotal = null;
    public TMP_Text totalSpent = null;
    public TMP_Text totalRemaining = null;

    public BudgetSheet budgetSheet = null;

    public void LoadSheet()
    {
        AppManager.instance.budgetManager.currentBudgetSheet = budgetSheet;
        AppManager.appMode = AppMode.SHEET;
        AppManager.instance.uiManager.sheetMenu.LoadPreviousBudgetSheet();
    }

    public void DeleteSheet()
    {
        AppManager.instance.memoryManager.DeleteBudgetSheet(budgetSheet.GetBudgetName());
        Destroy(this.gameObject);
    }
}