﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GoalProfileUI : MonoBehaviour
{
    public TMP_Text goalName = null;
    public TMP_Text goalTotal = null;
    public TMP_Text progressText = null;
    public Slider progressSlider = null;

    public Goal goal = null;

    public void DeleteGoal()
    {
        MemoryManager.DeleteGoalData(goal.GetGoalName());
        Destroy(this.gameObject);
    }
}