﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TransferGoalProfileUI : MonoBehaviour
{
    public TMP_InputField input = null;
    public TMP_Text goalNameText = null;
    public TMP_Text progress = null;
    public TMP_Text total = null;
    public TransferMenu menu = null;
    public Goal goal = null;
    public int priceInput = 0;

    public void SendInputToMenu()
    {
        int.TryParse(input.text, out priceInput);
        int tempProgress = goal.GetProgress() + priceInput;
        progress.text = StringIndex.GetCurrency(tempProgress);

        if (menu.Check())
        {
            menu.UpdateText();
        }
        else
        {
            input.text = string.Empty;
            priceInput = 0;
        }
    }

    public void SetGoalProgress()
    {
        int tempProgress = goal.GetProgress() + priceInput;
        goal.SetProgress(tempProgress);
    }
}
