﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TransferMenu : MonoBehaviour
{
    public TMP_Text remainingText = null;
    public Transform content = null;
    public TransferGoalProfileUI goalProfile = null;
    private List<TransferGoalProfileUI> profiles = new List<TransferGoalProfileUI>();
    private int remaining = 0;
    private int remainderMath = 0;
    
    public bool Check()
    {
        remaining = AppManager.instance.budgetManager.currentBudgetSheet.GetRemainingBudget();

        int total = 0;
        for (int i = 0; i < profiles.Count; i++)
            total += profiles[i].priceInput;

        remainderMath = remaining - total;

        if (remainderMath < 0)
            return false;
        else
            return true;
    }

    public void UpdateText()
    {
        remaining = AppManager.instance.budgetManager.currentBudgetSheet.GetRemainingBudget();
        Debug.Log(remaining);

        int total = 0;

        for (int i = 0; i < profiles.Count; i++)
            total += profiles[i].priceInput;
        Debug.Log(total);
        remainderMath = remaining - total;
        remainingText.text = StringIndex.GetCurrency(remainderMath);
    }

    public void ConfirmTransfer()
    {
        for(int i = 0; i < profiles.Count; i++)
        {
            profiles[i].SetGoalProgress();
            MemoryManager.SaveGoal(profiles[i].goal);
        }
        remaining = remainderMath;
        AppManager.instance.budgetManager.currentBudgetSheet.SetRemainingBudget(remaining);
        AppManager.instance.uiManager.sheetMenu.UpdateSheet();
        MemoryManager.SaveBudgetSheet(AppManager.instance.budgetManager.currentBudgetSheet);

        ClearMenu();
    }

    public void ClearMenu()
    {
        foreach (Transform t in content)
            Destroy(t.gameObject);
        profiles.Clear();
        remaining = 0;
        remainingText.text = string.Empty;
        this.gameObject.SetActive(false);
    }

    public void SetUpTransferMenu()
    {
        remaining = AppManager.instance.budgetManager.currentBudgetSheet.GetRemainingBudget();
        remainingText.text = StringIndex.GetCurrency(remaining);
        for(int i = 0; i < MemoryManager.GetFilesInGoalsFolder().Length; i++)
        {
            string file = MemoryManager.GetFilesInGoalsFolder()[i];
            Goal memoryGoal = MemoryManager.LoadGoal(file);

            GameObject go = Instantiate(goalProfile.gameObject, content);
            profiles.Add(go.GetComponent<TransferGoalProfileUI>());
            profiles[i].menu = this;
            profiles[i].goal = memoryGoal;
            profiles[i].goalNameText.text = memoryGoal.GetGoalName();
            profiles[i].total.text = StringIndex.GetCurrency(memoryGoal.GetGoalPrice());
            profiles[i].progress.text = StringIndex.GetCurrency(memoryGoal.GetProgress());
        }
    }
    
}
