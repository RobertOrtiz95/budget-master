﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject budgetMenu = null;
    public TMPro.TMP_InputField nameInput = null;
    public TMPro.TMP_InputField totalBudgetInput = null;

    public Transform profilesContent = null;

    public SheetProfileUI sheetProfilePrefab = null;

    public IEnumerator redraw = null;
    
    private void OnEnable()
    {
        budgetMenu.SetActive(false);
    }

    public void LoadPreviousSheets()
    {
        redraw = Redraw();
        StartCoroutine(redraw);
    }

    private IEnumerator Redraw()
    { 
        yield return new WaitForEndOfFrame();
        ClearInputs();

        foreach (Transform t in profilesContent)
            Destroy(t.gameObject);

        for (int i = 0; i < MemoryManager.GetFilesInSheetsFolder().Length; i++)
        {
            string file = MemoryManager.GetFilesInSheetsFolder()[i];
            BudgetSheet sheet = MemoryManager.LoadBudgetSheet(file);

            GameObject go = Instantiate(sheetProfilePrefab.gameObject, profilesContent);
            SheetProfileUI goProfile = go.GetComponent<SheetProfileUI>();
            goProfile.budgetSheet = sheet;
            goProfile.sheetName.text = sheet.GetBudgetName();
            goProfile.budgetTotal.text = StringIndex.GetTotalBudgetCurrency(sheet.GetTotalBudget());
            goProfile.totalSpent.text = StringIndex.GetTotalSpentCurrency(sheet.GetTotalSpent());
            goProfile.totalRemaining.text = StringIndex.GetRemainingBudgetCurrency(sheet.GetRemainingBudget());
        }
    }
        
    public void ClearInputs()
    {
        totalBudgetInput.text = string.Empty;
        nameInput.text = string.Empty;
    }

    public void CreateBudgetSheet()
    {
        int price = 0;
        int.TryParse(totalBudgetInput.text, out price);
        AppManager.instance.budgetManager.CreateNewSheet(price, nameInput.text);
        AppManager.appMode = AppMode.SHEET;
    }
}
