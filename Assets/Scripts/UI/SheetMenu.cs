﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SheetMenu : MonoBehaviour
{
    public GameObject expenseMenu = null;
    public GameObject expenseProfile = null;

    public Transform contentParent = null;

    public TMP_Text budgetNameText = null;
    public TMP_Text totalBudgetText = null;
    public TMP_Text totalSpentText = null;
    public TMP_Text remainingBudgetText = null;

    public TMP_InputField expenseReasonInput = null;
    public TMP_InputField expenseCostInput = null;

    public List<ExpenseProfileUI> expensesUI = new List<ExpenseProfileUI>();

    private void OnEnable()
    {
        expenseMenu.SetActive(false);
    }

    public void OnDisable()
    {
        ClearSheetMenu();
    }

    public void LoadPreviousBudgetSheet()
    {
        BudgetSheet curr = AppManager.instance.budgetManager.currentBudgetSheet;
        totalBudgetText.text = StringIndex.GetTotalBudgetCurrency(curr.GetTotalBudget());
        totalSpentText.text = StringIndex.GetTotalSpentCurrency(curr.GetTotalSpent());
        remainingBudgetText.text = StringIndex.GetRemainingBudgetCurrency(curr.GetRemainingBudget());
        
        foreach(ExpenseProfile expense in curr.GetAllExpenses())
        {
            GameObject go = Instantiate(expenseProfile);
            go.transform.SetParent(contentParent);

            ExpenseProfileUI goProfile = go.GetComponent<ExpenseProfileUI>();
            expensesUI.Add(goProfile);
            goProfile.cost.text = expense.expenseCost.ToString();
            goProfile.reason.text = expense.expenseName;
            goProfile.profile = expense;
        }
    }
    
    public void UpdateSheet()
    {
        BudgetSheet sheet = AppManager.instance.budgetManager.currentBudgetSheet;
        totalSpentText.text = StringIndex.GetTotalSpentCurrency(sheet.GetTotalSpent());
        remainingBudgetText.text = StringIndex.GetRemainingBudgetCurrency(sheet.GetRemainingBudget());
    }

    public void ClearInputFields()
    {
        expenseReasonInput.text = string.Empty;
        expenseCostInput.text = string.Empty;
    }

    public void CreateBudgetSheet()
    {
        if(AppManager.instance.budgetManager.currentBudgetSheet != null)
        {
            BudgetSheet budgetSheet = AppManager.instance.budgetManager.currentBudgetSheet;
            budgetNameText.text = budgetSheet.GetBudgetName();
            totalBudgetText.text = StringIndex.GetTotalBudgetCurrency(budgetSheet.GetTotalBudget());
            totalSpentText.text = StringIndex.GetTotalSpentCurrency(budgetSheet.GetTotalSpent());
            remainingBudgetText.text = StringIndex.GetRemainingBudgetCurrency(budgetSheet.GetTotalBudget());
        }
    }

    public void ClearSheetMenu()
    {
        budgetNameText.text = string.Empty;
        totalBudgetText.text = string.Empty;
        totalSpentText.text = string.Empty;
        remainingBudgetText.text = string.Empty;

        for (int i = 0; i < expensesUI.Count; i++)
            Destroy(expensesUI[i].gameObject);

        expensesUI.Clear();

        //AppManager.instance.uiManager.mainMenu.ClearInputs();

        //AppManager.instance.uiManager.mainMenu.LoadPreviousSheets();

        AppManager.instance.budgetManager.currentBudgetSheet = null;        
    }

    public void RemoveLastBudget()
    {
        if(expensesUI.Count == 0)
            return;

        ExpenseProfileUI profile = expensesUI[expensesUI.Count - 1];
        expensesUI.Remove(profile);
        AppManager.instance.budgetManager.currentBudgetSheet.RemoveLastExpense();
        Destroy(profile.gameObject);
        UpdateSheet();
        Save();
    }

    public void CreateExpense()
    {
        if (expenseReasonInput.text == string.Empty)
            return;
        else if (expenseCostInput.text == string.Empty)
            return;

        GameObject go = Instantiate(expenseProfile);
        go.transform.SetParent(contentParent);

        ExpenseProfileUI goProfile = go.GetComponent<ExpenseProfileUI>();
        expensesUI.Add(goProfile);
        goProfile.cost.text = expenseCostInput.text;
        goProfile.reason.text = expenseReasonInput.text;
        goProfile.CreateProfile();
        Save();
    }

    public void Save()
    {
        BudgetSheet currentSheet = AppManager.instance.budgetManager.currentBudgetSheet;
        MemoryManager.SaveBudgetSheet(currentSheet);
    }
}
