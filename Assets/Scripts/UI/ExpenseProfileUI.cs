﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExpenseProfileUI : MonoBehaviour
{
    public TMP_Text cost;
    public TMP_Text reason;

    public ExpenseProfile profile = null;

    public void CreateProfile()
    {
        int.TryParse(cost.text, out profile.expenseCost);
        profile.expenseName = reason.text;
        AppManager.instance.budgetManager.AddNewExpense(profile);
    }
}