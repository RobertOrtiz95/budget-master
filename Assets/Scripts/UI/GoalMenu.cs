﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoalMenu : MonoBehaviour
{
    public GameObject goalMenu = null;
    public TMP_InputField goalNameInput = null;
    public TMP_InputField goalTotalInput = null;

    public GoalProfileUI goalProfile = null;

    public Transform goalContent = null;

    private void Start()
    {
        LoadGoals();
    }

    public void LoadGoals()
    {
        foreach (Transform t in goalContent)
            Destroy(t.gameObject);

        for(int i = 0; i < MemoryManager.GetFilesInGoalsFolder().Length; i++)
        {
            string file = MemoryManager.GetFilesInGoalsFolder()[i];
            Goal memoryGoal = MemoryManager.LoadGoal(file);
            GameObject go = Instantiate(goalProfile.gameObject, goalContent);
            GoalProfileUI goGoal = go.GetComponent<GoalProfileUI>();
            goGoal.goal = memoryGoal;
            goGoal.goalName.text = memoryGoal.GetGoalName();
            goGoal.goalTotal.text = StringIndex.GetCurrency(memoryGoal.GetGoalPrice());
            goGoal.progressText.text = StringIndex.GetCurrency(memoryGoal.GetProgress());
            goGoal.progressSlider.maxValue = memoryGoal.GetGoalPrice();
            goGoal.progressSlider.value = memoryGoal.GetProgress();
        }
    }

    public void ClearInputs()
    {
        goalNameInput.text = string.Empty;
        goalTotalInput.text = string.Empty;
    }

    public void CreateGoal()
    {
        int total = 0;
        int.TryParse(goalTotalInput.text, out total);
        Goal goal = new Goal(goalNameInput.text, total);
        MemoryManager.SaveGoal(goal);
        goalMenu.SetActive(false);
        LoadGoals();
        ClearInputs();
    }
}
